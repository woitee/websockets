package cz.woitee.websockets;

import java.io.IOException;
import java.io.InputStream;

public class WebSocketFrame {
	public int bytesRead;
	public byte[] bytes = null;
	public int opcode;
	public boolean masked;
	protected WebSocketFrame(int bytesRead) {
		this.bytesRead = bytesRead;
		this.opcode = -1;
	}
	protected WebSocketFrame(byte[] bytes, int opcode, boolean masked) {
		//not bytes read from socket, but bytes that are there for the user to read
		//used, so the user may keep using WebSockets the same as Sockets
		this.bytesRead = bytes.length;
		this.bytes = bytes;
		this.opcode = opcode;
		this.masked = masked;
	}
	
	
	// STATIC METHODS, used as a library
	public static boolean appendNewlineToIncoming = true;
	
	//static methods to create/parse WebSocket frames
	public static byte[] construct(byte[] contents) {
		return construct(contents, 0, contents.length);
	}
	public static byte[] construct(byte[] contents, int offset, int length) {
		return construct(contents, offset, length, 1);
	}
	public static byte[] construct(byte[] contents, int offset, int length, int opcode) {
		byte[] frame; int payloadStart;
		if (length <= 125) {
			frame = new byte[length + 2];
			frame[1] = (byte)length;
			payloadStart = 2;
		} else if (contents.length < 2<<16) { //length fits in unsigned 16-bit integer
			frame = new byte[length + 4];
			frame[1] = 126;
			frame[2] = (byte)(length >> 8);
			frame[3] = (byte)length;
			payloadStart = 4;
		} else { //length fits in a 32-bit integer
			frame = new byte[length + 8];
			frame[1] = 127;
			for (int i = 2; i <= 5; ++i)
				frame[i] = 0;
			frame[6] = (byte)(length >> 24);
			frame[7] = (byte)(length >> 16);
			frame[8] = (byte)(length >> 8);
			frame[9] = (byte)length;
			payloadStart = 8;
		}
		frame[0] = (byte) ((byte)128 | (byte)opcode); //FIN = 1, RSV1-3 = 0, Opcode = opcode
		int i = offset, j = payloadStart;
		for ( ; i < length; ++i, ++j) {
			frame[j] = contents[i];
		}
		return frame;
	}
	
	private static void forceRead(InputStream input, byte[] buf) throws IOException {
		forceRead(input, buf, 0, buf.length);
	}
	private static void forceRead(InputStream input, byte[] buf, int off, int len) throws IOException {
		int read = input.read(buf, off, len);
		if (read == -1) { throw new IOException("Unexpected end of file."); }
		while (read < len) {
			int n = input.read(buf, off + read, len - read);
			if (n == -1) { throw new IOException("Unexpected end of file."); }
			read += n;
		}
	}
	public static WebSocketFrame readFrame(InputStream input) throws WebSocketException, IOException {
		@SuppressWarnings("unused")
		boolean fin;
		//read FIN and OPCODE
		int read = input.read();
		if (read == -1) { return new WebSocketFrame(-1); }
		byte b = (byte)read;
		fin = ((b & 128) >> 7) == 1;
		//ignoring extensions bits RSV1-RSV3
		byte opcode = (byte)(b & (byte)15);
		
		//read MASK and PAYLOAD LENGTH
		read = input.read();
		if (read == -1) { throw new IOException("Unexpected end of file."); }
		b = (byte)read;
		boolean mask = ((b & 128) >> 7) == 1;
		long payloadLength = (b & 127);
		if (payloadLength == 126) {
			byte[] buf = new byte[2];
			forceRead(input, buf);
			payloadLength = (buf[0] << 8L) & 0x0000_0000_0000_ff00L |
				        			buf[1] & 0x0000_0000_0000_00ffL;
		} else if (payloadLength == 127) {
			byte[] buf = new byte[8];
			forceRead(input, buf);
			payloadLength = (buf[0] << 56L) & 0xff00_0000_0000_0000L |
							(buf[1] << 48L) & 0x00ff_0000_0000_0000L |
							(buf[2] << 40L) & 0x0000_ff00_0000_0000L |
							(buf[3] << 32L) & 0x0000_00ff_0000_0000L |
							(buf[4] << 24L) & 0x0000_0000_ff00_0000L |
							(buf[5] << 16L) & 0x0000_0000_00ff_0000L |
							(buf[6] <<  8L) & 0x0000_0000_0000_ff00L |
									buf[7]  & 0x0000_0000_0000_00ffL;
		}
		//payload length is theoretically unsigned, but NOONE is going to send message
		//with MAX_LONG bytes... well further retype this to int, so we even cannot
		//receive messages larger then MAX_INT
		//(please dont send 2GB unsplit messages over websockets)
		//throwing Exception just in case
		if (payloadLength > (long)Integer.MAX_VALUE) {
			throw new WebSocketException("Payload length exceeds "+Integer.MAX_VALUE+" bytes.");
		}
		
		//read MASKING-KEY
		byte[] key = null;
		if (mask) {
			key = new byte[4];
			forceRead(input, key);
		}

		//read PAYLOAD
		byte[] payload;
		if (appendNewlineToIncoming) {
			//default
			payload = new byte[(int)payloadLength + 2]; //adding a \r\n at end of frame
			forceRead(input, payload, 0, (int)payloadLength);
			payload[payload.length - 2] = '\r';
			payload[payload.length - 1] = '\n';
		} else { 
			payload = new byte[(int)payloadLength];
			forceRead(input, payload);
			
		}
		
		//decode if necessary
		if (mask) {
			for (int i = 0; i < payloadLength; ++i) {
				payload[i] ^= key[i % 4];
			}
		}
		
		return new WebSocketFrame(payload, opcode, mask);
	}
}
