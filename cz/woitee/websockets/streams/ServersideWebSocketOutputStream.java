package cz.woitee.websockets.streams;

import java.io.*;
import java.net.Socket;

import cz.woitee.websockets.WebSocketFrame;

public class ServersideWebSocketOutputStream extends FilterOutputStream{
	
	protected Socket socket;
	
	public ServersideWebSocketOutputStream(Socket socket) throws IOException {
		super(socket.getOutputStream());
		this.socket = socket;
	}

	@Override
	public void write(int b) throws IOException {
		write(new byte[] {(byte)b});
	}
	
	@Override
	public void write(byte[] b) throws IOException {
		out.write(WebSocketFrame.construct(b, 0, b.length));
	}
	
	@Override
	public void write(byte[] b, int off, int len) throws IOException {
		out.write(WebSocketFrame.construct(b, off, len));
	}
}
