package cz.woitee.websockets.utils;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.xml.bind.DatatypeConverter;

/**
 * Representation of a UTF-8 string, hidden in a byte array, always excluding any null bytes.
 * @author woitee
 */
public class UTF8String {
	protected byte[] data;
	public UTF8String() {
		data = new byte[0];
	}
	public UTF8String(byte[] data) {
		this.data = data;
		trimNulls();
	}
	public UTF8String(String s) throws UnsupportedEncodingException {
		data = s.getBytes("UTF-8");
		trimNulls();
	}
	public byte[] array() {
		return data;
	}
	public int length() {
		return data.length;
	}
	public UTF8String concat(UTF8String r) {
		int countNulls = 0;
		for (byte b: r.data) {
			if (b==0) ++countNulls;
		}
		byte[] ret = new byte[data.length + r.data.length - countNulls];
		int ptr = 0;
		for (byte b: data) {
			ret[ptr++] = b;
		}
		for (byte b: r.data) {
			if (b != 0)
				ret[ptr++] = b;
		}
		data = ret;
		return this;
	}
	public UTF8String trimNulls() {
		data = new UTF8String().concat(this).array();
		return this;
	}
	public String hexFormat() {
		StringBuilder sb = new StringBuilder();
		for (byte b: data) {
			sb.append(byteToHex(b));
		}
		return sb.toString();
	}
	public static String byteToHex(byte b) {
		// Returns hex String representation of byte b
		char hexDigit[] = {
	       '0', '1', '2', '3', '4', '5', '6', '7',
	       '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'
	    };
	    char[] array = { hexDigit[(b >> 4) & 0x0f], hexDigit[b & 0x0f] };
	    return new String(array);
	}
	public UTF8String toSHA1() throws NoSuchAlgorithmException{
		MessageDigest mDigest = MessageDigest.getInstance("SHA1");
        data = mDigest.digest(data);
        return this;
	}
	public UTF8String toBase64() throws UnsupportedEncodingException {
		data = new UTF8String(DatatypeConverter.printBase64Binary(data)).array();
		trimNulls();
		return this;
	}
	public String toString() {
		try {
			return new String(data, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			return null;
		}
	}
}
