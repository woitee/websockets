package cz.woitee.websockets;

import java.io.IOException;

public class WebSocketException extends IOException {
	private static final long serialVersionUID = 1L;
	public WebSocketException() {}
	public WebSocketException(String msg) {
		super(msg);
	}
	public WebSocketException(Throwable cause) {
		super(cause);
	}
	public WebSocketException(String msg, Throwable cause) {
		super(msg, cause);
	}
}
