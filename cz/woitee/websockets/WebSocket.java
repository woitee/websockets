package cz.woitee.websockets;

import java.io.*;
import java.net.Socket;

/**
 * Predecessor of all WebSocket sockets. This class is just a wrapper of java.net.Socket,
 * and can be easily overloaded to send encrypted and/or specifically formatted data.
 * 
 * @author woitee
 *
 */

public abstract class WebSocket {
	protected Socket socket;
	public WebSocket (Socket socket) {
		this.socket = socket;
	}

	public Socket getUnderlyingSocket() {
		return socket;
	}
	
	public abstract InputStream getInputStream() throws IOException;
	public abstract OutputStream getOutputStream() throws IOException;
	
	public void close() throws IOException {
		//CLOSING HANDSHAKE
		try {
			//send closing frame
			getOutputStream().write(WebSocketFrame.construct(new byte[0], 0, 0, 0x8));
		} finally {
			socket.close();
		}
	}
}
